from rest_framework import serializers
from rest_framework.validators import UniqueValidator, UniqueTogetherValidator

from .models import Continent, Country, Region, City, ZipCode, GeoName, Coordinates, IpAddress, Language, Url


class ContinentSerializer(serializers.Serializer):
    continent_code = serializers.CharField(max_length=2, validators=[UniqueValidator(queryset=Continent.objects.all())])
    continent_name = serializers.CharField(max_length=20)

    def create(self, validated_data):
        return Continent.objects.create(**validated_data)


class GeoNameSerializer(serializers.Serializer):
    class LocationNestedSerializer(serializers.Serializer):
        geoname_id = serializers.IntegerField(validators=[UniqueValidator(queryset=GeoName.objects.all())])

    location = LocationNestedSerializer(source='*')

    def create(self, validated_data):
        return GeoName.objects.create(**validated_data)


class LanguageNestedSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=2)


class CountrySerializer(serializers.Serializer):
    class LocationNestedSerializer(serializers.Serializer):
        capital = serializers.CharField(max_length=100)
        country_flag = serializers.URLField()
        country_flag_emoji = serializers.CharField(max_length=100)
        country_flag_emoji_unicode = serializers.CharField(max_length=100)
        calling_code = serializers.CharField(max_length=10)
        is_eu = serializers.BooleanField()
        languages = serializers.ListField(child=LanguageNestedSerializer())

    continent_code = serializers.CharField(max_length=2)
    location = LocationNestedSerializer(source='*')
    country_code = serializers.CharField(max_length=2, validators=[UniqueValidator(queryset=Country.objects.all())])
    country_name = serializers.CharField(max_length=100)

    def create(self, validated_data):
        continent_code = validated_data['continent_code']
        continent = Continent.objects.get(continent_code=continent_code)
        validated_data['continent'] = continent
        del validated_data['continent_code']
        languages = validated_data['languages'][:]
        del validated_data['languages']

        country_object = Country.objects.create(**validated_data)
        for language in languages:
            language = Language.objects.get(code=language['code'])
            country_object.languages.add(language)
        return country_object


class RegionSerializer(serializers.Serializer):
    class Meta:
        validators = [
            UniqueTogetherValidator(
                queryset=Region.objects.all(),
                fields=['region_code', 'region_name']
            )
        ]

    country_code = serializers.CharField(max_length=2)
    region_code = serializers.CharField(max_length=100)
    region_name = serializers.CharField(max_length=100)

    def create(self, validated_data):
        country_code = validated_data['country_code']
        country = Country.objects.get(country_code=country_code)
        validated_data['country'] = country
        del validated_data['country_code']
        return Region.objects.create(**validated_data)


class CitySerializer(serializers.Serializer):
    region_code = serializers.CharField(max_length=100)
    city = serializers.CharField(max_length=100, validators=[UniqueValidator(queryset=City.objects.all())])

    def create(self, validated_data):
        region_code = validated_data['region_code']
        region = Region.objects.get(region_code=region_code)
        validated_data['region'] = region
        del validated_data['region_code']
        return City.objects.create(**validated_data)


class ZipCodeSerializer(serializers.Serializer):
    zip = serializers.CharField(max_length=18, validators=[UniqueValidator(queryset=ZipCode.objects.all())])

    def create(self, validated_data):
        return ZipCode.objects.create(**validated_data)


class CoordinatesSerializer(serializers.Serializer):
    class Meta:
        validators = [
            UniqueTogetherValidator(
                queryset=Coordinates.objects.all(),
                fields=['latitude', 'longitude']
            )
        ]

    class LocationNestedSerializer(serializers.Serializer):
        geoname_id = serializers.IntegerField()

    city = serializers.CharField(max_length=100)
    zip = serializers.CharField(max_length=18, allow_null=True)
    location = LocationNestedSerializer(source='*')
    latitude = serializers.DecimalField(max_digits=25, decimal_places=22)
    longitude = serializers.DecimalField(max_digits=25, decimal_places=22)

    def create(self, validated_data):
        city = validated_data['city']
        city = City.objects.get(city=city)
        validated_data['city'] = city
        geoname_id = validated_data['geoname_id']
        geoname = GeoName.objects.get(geoname_id=geoname_id)
        validated_data['geoname'] = geoname
        zip = validated_data['zip']
        if zip:
            zip = ZipCode.objects.get(zip=zip)
            validated_data['zip'] = zip
        del validated_data['geoname_id']
        return Coordinates.objects.create(**validated_data)


class UrlSerializer(serializers.Serializer):
    url = serializers.CharField(max_length=100, validators=[UniqueValidator(queryset=Url.objects.all())])

    def create(self, validated_data):
        return Url.objects.create(**validated_data)


class IpSerializer(serializers.Serializer):
    latitude = serializers.DecimalField(max_digits=25, decimal_places=22)
    longitude = serializers.DecimalField(max_digits=25, decimal_places=22)
    ip = serializers.IPAddressField(validators=[UniqueValidator(queryset=IpAddress.objects.all())])
    type = serializers.CharField(max_length=4)
    url = serializers.CharField(max_length=100, allow_null=True, allow_blank=True)

    def create(self, validated_data):
        latitude = validated_data['latitude']
        longitude = validated_data['longitude']
        coordinates = Coordinates.objects.get(latitude=latitude, longitude=longitude)
        validated_data['coordinates'] = coordinates
        url = validated_data['url']
        if url:
            url = Url.objects.get(url=url)
            validated_data['url'] = url
        del validated_data['latitude']
        del validated_data['longitude']
        return IpAddress.objects.create(**validated_data)


class LanguageNestedUniqueSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=2)
    name = serializers.CharField(max_length=100)
    native = serializers.CharField(max_length=100)


class LanguageSerializer(serializers.Serializer):
    class LocationNestedSerializer(serializers.Serializer):
        languages = serializers.ListField(child=LanguageNestedUniqueSerializer())

    location = LocationNestedSerializer(source='*')

    def create(self, validated_data):
        for language in validated_data['languages']:
            try:
                obj = Language.objects.get(code=language['code'])
            except Language.DoesNotExist:
                obj = Language.objects.create(**dict(language))
        return obj


class Deserializer(serializers.Serializer):

    def to_representation(self, instance):
        languages = [{"language_code": language.code, "language_name": language.name,
                      "language_native": language.native}
                     for language in instance.coordinates.city.region.country.languages.all()]
        return {
            'ip': {
                'ip_address': instance.ip,
                'type': instance.type,
            },
            'url': instance.url.url if instance.url else None,
            'coordinates': {
                'latitude': instance.coordinates.latitude,
                'longitude': instance.coordinates.longitude,
            },
            'geoname_id': instance.coordinates.geoname.geoname_id,
            'city': instance.coordinates.city.city,
            'zip_code': instance.coordinates.zip.zip if instance.coordinates.zip else None,
            'region': {
                'region_code': instance.coordinates.city.region.region_code,
                'region_name': instance.coordinates.city.region.region_name,
            },
            'country': {
                'country_code': instance.coordinates.city.region.country.country_code,
                'country_name': instance.coordinates.city.region.country.country_name,
                'capital': instance.coordinates.city.region.country.capital,
                'country_flag': instance.coordinates.city.region.country.country_flag,
                'country_flag_emoji': instance.coordinates.city.region.country.country_flag_emoji,
                'country_flag_emoji_unicode': instance.coordinates.city.region.country.country_flag_emoji_unicode,
                'calling_code': instance.coordinates.city.region.country.calling_code,
                'is_eu': instance.coordinates.city.region.country.is_eu,
                'languages': languages,
            },
            'continent': {
                'continent_code': instance.coordinates.city.region.country.continent.continent_code,
                'continent_name': instance.coordinates.city.region.country.continent.continent_name,
            },
        }
