from django.urls import path
from . import views

urlpatterns = [
    path('', views.GeoGetMainView.as_view(), name='main'),
]