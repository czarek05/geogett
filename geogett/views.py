import io
import socket

import validators
from django.http import JsonResponse
import requests
from rest_framework.exceptions import ParseError
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from .constants import resp_not_json, resp_invalid_body, resp_unknown_url, resp_unknown_ip, resp_empty_body, \
    resp_post_known_ip, resp_post_known_url, resp_ip_stack_bad, resp_invalid_ip, resp_invalid_url
from .models import IpAddress
from .serializers import ContinentSerializer, CountrySerializer, RegionSerializer, CitySerializer, ZipCodeSerializer, \
    GeoNameSerializer, CoordinatesSerializer, IpSerializer, LanguageSerializer, Deserializer, UrlSerializer
import sys


def is_valid_ipv4_address(address):
    try:
        socket.inet_pton(socket.AF_INET, address)
    except AttributeError:
        try:
            socket.inet_aton(address)
        except socket.error:
            return False
        return address.count('.') == 3
    except socket.error:
        return False

    return True


def is_valid_ipv6_address(address):
    try:
        socket.inet_pton(socket.AF_INET6, address)
    except socket.error:
        return False
    return True


def is_valid_ip(address):
    return is_valid_ipv4_address(address) or is_valid_ipv6_address(address)


class GeoGetMainView(APIView):
    # turn off JWT auth for testing purposes
    permission_classes = () if 'test' in sys.argv else (IsAuthenticated,)

    def get(self, request):
        if request.body:
            stream = io.BytesIO(request.body)
            try:
                requested_ip_or_url = JSONParser().parse(stream)
            except ParseError:
                return JsonResponse({'info': resp_not_json, 'status_code': 400}, status=400)
            if len(requested_ip_or_url) != 1 or ('url' not in requested_ip_or_url and 'ip' not in requested_ip_or_url):
                return JsonResponse({'info': resp_invalid_body, 'status_code': 400},
                                    status=400)
        if request.body:
            try:
                if 'ip' in requested_ip_or_url:
                    if not is_valid_ip(requested_ip_or_url['ip']):
                        return JsonResponse({'info': resp_invalid_ip, 'status_code': 400}, status=400)
                    geogett_data = IpAddress.objects.filter(ip=requested_ip_or_url['ip'])
                else:
                    if not validators.domain(requested_ip_or_url['url']):
                        return JsonResponse({'info': resp_invalid_url, 'status_code': 400}, status=400)
                    geogett_data = IpAddress.objects.filter(url__url=requested_ip_or_url['url'])
                if not geogett_data:
                    return JsonResponse({'info': resp_unknown_url, 'status_code': 404}, status=404)
            except IpAddress.DoesNotExist:
                return JsonResponse({'info': resp_unknown_ip, 'status_code': 404}, status=404)
        else:
            geogett_data = IpAddress.objects.all()
        deserializer = Deserializer(geogett_data, many=True)
        return JsonResponse(deserializer.data, safe=False)

    def post(self, request):
        if request.body:
            stream = io.BytesIO(request.body)
            try:
                requested_ip_or_url = JSONParser().parse(stream)
            except ParseError:
                return JsonResponse({'info': resp_not_json, 'status_code': 400}, status=400)
            if len(requested_ip_or_url) != 1 or ('url' not in requested_ip_or_url and 'ip' not in requested_ip_or_url):
                return JsonResponse({'info': resp_invalid_body, 'status_code': 400},
                                    status=400)
        else:
            return JsonResponse({'info': resp_empty_body, 'status_code': 400},
                                status=400)
        if 'ip' in requested_ip_or_url:
            try:
                if not is_valid_ip(requested_ip_or_url['ip']):
                    return JsonResponse({'info': resp_invalid_ip, 'status_code': 400}, status=400)
                geogett_data = IpAddress.objects.get(ip=requested_ip_or_url['ip'])
                return JsonResponse(
                    {'info': resp_post_known_ip, 'status_code': 409}, status=409)
            except IpAddress.DoesNotExist:
                pass
        if 'url' in requested_ip_or_url:
            if not validators.domain(requested_ip_or_url['url']):
                return JsonResponse({'info': resp_invalid_url, 'status_code': 400}, status=400)
        url = "http://api.ipstack.com/{ip_or_url}?access_key=aaba64edcc3f00160e632684f3d378ec".format(
            ip_or_url=requested_ip_or_url['ip'] if 'ip' in requested_ip_or_url else requested_ip_or_url['url'])
        response = requests.get(url)
        if '"error":{"code"' in str(response.content) or response.status_code != 200:
            return JsonResponse({'info': resp_ip_stack_bad, 'status_code': 500}, status=500)
        stream = io.BytesIO(response.content)
        ipstack_data = JSONParser().parse(stream)
        if 'url' in requested_ip_or_url:
            try:
                IpAddress.objects.get(ip=ipstack_data['ip'])
                return JsonResponse(
                    {'info': resp_post_known_url, 'status_code': 409}, status=409)
            except IpAddress.DoesNotExist:
                pass
        ipstack_data['url'] = requested_ip_or_url['url'] if 'url' in requested_ip_or_url else None
        continent_serializer = ContinentSerializer(data=ipstack_data)
        if continent_serializer.is_valid():
            continent_serializer.save()
        geoname_serializer = GeoNameSerializer(data=ipstack_data)
        if geoname_serializer.is_valid():
            geoname_serializer.save()
        language_serializer = LanguageSerializer(data=ipstack_data)
        if language_serializer.is_valid():
            language_serializer.save()
        country_serializer = CountrySerializer(data=ipstack_data)
        if country_serializer.is_valid():
            country_serializer.save()
        region_serializer = RegionSerializer(data=ipstack_data)
        if region_serializer.is_valid():
            region_serializer.save()
        city_serializer = CitySerializer(data=ipstack_data)
        if city_serializer.is_valid():
            city_serializer.save()
        zip_code_serializer = ZipCodeSerializer(data=ipstack_data)
        if zip_code_serializer.is_valid():
            zip_code_serializer.save()
        coordinates_serializer = CoordinatesSerializer(data=ipstack_data)
        if coordinates_serializer.is_valid():
            coordinates_serializer.save()
        url_serializer = UrlSerializer(data=ipstack_data)
        if url_serializer.is_valid():
            url_serializer.save()
        ip_serializer = IpSerializer(data=ipstack_data)
        if ip_serializer.is_valid():
            ip_serializer.save()
        return JsonResponse({'info': 'GeoGett added geolocation data for {ip}'.format(ip=ipstack_data['ip']),
                             'status_code': 200})

    def delete(self, request):
        if request.body:
            stream = io.BytesIO(request.body)
            try:
                requested_ip_or_url = JSONParser().parse(stream)
            except ParseError:
                return JsonResponse({'info': resp_not_json, 'status_code': 400}, status=400)
            if len(requested_ip_or_url) != 1 or ('url' not in requested_ip_or_url and 'ip' not in requested_ip_or_url):
                return JsonResponse({'info': resp_invalid_body, 'status_code': 400},
                                    status=400)
        else:
            return JsonResponse({'info': resp_empty_body, 'status_code': 400}, status=400)
        if 'ip' in requested_ip_or_url:
            try:
                if not is_valid_ip(requested_ip_or_url['ip']):
                    return JsonResponse({'info': resp_invalid_ip, 'status_code': 400}, status=400)
                geogett_data = IpAddress.objects.get(ip=requested_ip_or_url['ip'])
            except IpAddress.DoesNotExist:
                return JsonResponse({'info': 'GeoGett can not delete geolocation data for {ip}.'
                                             ' Data for {ip} does not exist'.format(ip=requested_ip_or_url['ip']),
                                     'status_code': 404}, status=404)
        else:
            if not validators.domain(requested_ip_or_url['url']):
                return JsonResponse({'info': resp_invalid_url, 'status_code': 400}, status=400)
            geogett_data = IpAddress.objects.filter(url__url=requested_ip_or_url['url'])
            if not geogett_data:
                return JsonResponse({'info': 'GeoGett can not delete geolocation data for {url}.'
                                             ' Data for {url} does not exist'.format(url=requested_ip_or_url['url']),
                                     'status_code': 404}, status=404)
        geogett_data.delete()
        ip_or_url = requested_ip_or_url['ip'] if 'ip' in requested_ip_or_url else requested_ip_or_url['url']
        return JsonResponse(
            {'info': 'GeoGett deleted geolocation data related to {ip_or_url}'.format(ip_or_url=ip_or_url),
             'status_code': 200})
