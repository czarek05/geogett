from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models
from django.dispatch import receiver


class User(AbstractBaseUser):
    is_active = models.BooleanField(default=True)


class GeoName(models.Model):
    geoname_id = models.IntegerField(unique=True)


class Continent(models.Model):
    continent_code = models.CharField(max_length=2, unique=True)
    continent_name = models.CharField(max_length=20)


class Country(models.Model):
    languages = models.ManyToManyField('Language')
    continent = models.ForeignKey(Continent, on_delete=models.CASCADE)
    country_code = models.CharField(max_length=2, unique=True)
    country_name = models.CharField(max_length=100)
    capital = models.CharField(max_length=100)
    country_flag = models.URLField()
    country_flag_emoji = models.CharField(max_length=100)
    country_flag_emoji_unicode = models.CharField(max_length=100)
    calling_code = models.CharField(max_length=10)
    is_eu = models.BooleanField()


class Language(models.Model):
    countries = models.ManyToManyField('Country', through=Country.languages.through)
    code = models.CharField(max_length=2)
    name = models.CharField(max_length=100)
    native = models.CharField(max_length=100)


class Region(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    region_code = models.CharField(max_length=100)
    region_name = models.CharField(max_length=100)


class City(models.Model):
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    city = models.CharField(max_length=100)


class ZipCode(models.Model):
    zip = models.CharField(max_length=18, unique=True)


class Coordinates(models.Model):
    geoname = models.ForeignKey(GeoName, on_delete=models.CASCADE)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    zip = models.ForeignKey(ZipCode, on_delete=models.CASCADE, null=True)
    latitude = models.DecimalField(max_digits=25, decimal_places=22)
    longitude = models.DecimalField(max_digits=25, decimal_places=22)


class Url(models.Model):
    url = models.CharField(max_length=100, unique=True)


class IpAddress(models.Model):
    coordinates = models.ForeignKey(Coordinates, on_delete=models.CASCADE)
    url = models.ForeignKey(Url, on_delete=models.CASCADE, null=True)
    ip = models.GenericIPAddressField(unique=True)
    type = models.CharField(max_length=4)


@receiver(models.signals.post_delete, sender=IpAddress)
def delete_url_coordinates(sender, instance, **kwargs):
    try:
        if instance.url:
            if not IpAddress.objects.filter(url=instance.url):
                instance.url.delete()
        if not IpAddress.objects.filter(coordinates=instance.coordinates):
            instance.coordinates.delete()
    except Url.DoesNotExist:
        pass


@receiver(models.signals.post_delete, sender=Coordinates)
def delete_geoname_zip_city(sender, instance, **kwargs):
    if not Coordinates.objects.filter(geoname=instance.geoname):
        instance.geoname.delete()
    if not Coordinates.objects.filter(zip=instance.zip):
        if instance.zip:
            instance.zip.delete()
    if not Coordinates.objects.filter(city=instance.city):
        instance.city.delete()


@receiver(models.signals.post_delete, sender=City)
def delete_region(sender, instance, **kwargs):
    if not City.objects.filter(region=instance.region):
        instance.region.delete()


@receiver(models.signals.post_delete, sender=Region)
def delete_country_languages(sender, instance, **kwargs):
    if not Region.objects.filter(country=instance.country):
        for language in instance.country.languages.all():
            if language.countries.all().count() == 1:
                language.delete()
        instance.country.delete()


@receiver(models.signals.post_delete, sender=Country)
def delete_continent(sender, instance, **kwargs):
    if not Country.objects.filter(continent=instance.continent):
        instance.continent.delete()
