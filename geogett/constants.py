resp_not_json = 'Request body is invalid. Please provide proper request body. Request body should be JSON.'
resp_invalid_body = 'Request body is invalid. Please provide proper request body. ' \
                    'Request body may contain only one field: ip or url.'
resp_unknown_url = "GeoGett doesn't know requested url. Try performing POST with that url."
resp_unknown_ip = "GeoGett doesn't know requested ip. Try performing POST with that ip."
resp_empty_body = 'Request body is empty. Please provide proper request body. ' \
                  'Request body may contain only one field: ip or url'
resp_post_known_ip = 'GeoGett already know geolocation for requested ip. Try performing GET with that ip.'
resp_post_known_url = 'GeoGett already know obtained ip. Try performing GET with that url or repeat POST, ' \
                      'to try to obtain another ip.'
resp_ip_stack_bad = 'Error with geolocation data provider. Please try again later'
resp_invalid_ip = 'Provided ip is invalid.'
resp_invalid_url = 'Provided url is invalid.'

# tests
not_json_body = 'aaa'
invalid_body = {'url': 'google.com', 'unnecessary_field': 1}
valid_body = {'ip': '5.134.213.82'}
valid_body_url = {'url': 'allegro.pl'}

#   "ip": "5.134.213.82"
ip_stack_mock_response = b'{"ip":"5.134.213.82","type":"ipv4","continent_code":"EU","continent_name":"Europe",' \
                         b'"country_code":"PL","country_name":"Poland","region_code":"KP",' \
                         b'"region_name":"Kujawsko-Pomorskie","city":"Grudzi\\u0105dz","zip":"86-132",' \
                         b'"latitude":53.50477981567383,"longitude":18.625879287719727,' \
                         b'"location":{"geoname_id":3098218,"capital":"Warsaw",' \
                         b'"languages":[{"code":"pl","name":"Polish","native":"Polski"}],' \
                         b'"country_flag":"http:\\/\\/assets.ipstack.com\\/flags\\/pl.svg",' \
                         b'"country_flag_emoji":"\\ud83c\\uddf5\\ud83c\\uddf1",' \
                         b'"country_flag_emoji_unicode":"U+1F1F5 U+1F1F1","calling_code":"48","is_eu":true}}'

ip_stack_bad_mock_response = b'{"success":false,"error":{"code":101,"type":"invalid_access_key",' \
                        b'"info":"You have not supplied a valid API Access Key. ' \
                        b'[Technical Support: support@apilayer.com]"}}'

geogett_proper_get_response = b'[{"ip": {"ip_address": "5.134.213.82", "type": "ipv4"}, "url": null, ' \
                              b'"coordinates": {"latitude": "53.5047798156738300000000", ' \
                              b'"longitude": "18.6258792877197270000000"}, "geoname_id": 3098218, ' \
                              b'"city": "Grudzi\\u0105dz", "zip_code": "86-132", ' \
                              b'"region": {"region_code": "KP", "region_name": "Kujawsko-Pomorskie"}, ' \
                              b'"country": {"country_code": "PL", "country_name": "Poland", "capital": "Warsaw", ' \
                              b'"country_flag": "http://assets.ipstack.com/flags/pl.svg", ' \
                              b'"country_flag_emoji": "\\ud83c\\uddf5\\ud83c\\uddf1", ' \
                              b'"country_flag_emoji_unicode": "U+1F1F5 U+1F1F1", ' \
                              b'"calling_code": "48", "is_eu": true, ' \
                              b'"languages": [{"language_code": "pl", "language_name": "Polish", ' \
                              b'"language_native": "Polski"}]}, ' \
                              b'"continent": {"continent_code": "EU", "continent_name": "Europe"}}]'
