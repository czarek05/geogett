import json
from unittest.mock import patch, MagicMock

from django.test import TestCase
from .constants import *


class GeoGettTests(TestCase):

    def test_not_json_body(self):
        response = self.client.generic('GET', path='/geogett/', data=not_json_body)
        assert resp_not_json in str(response.content)
        assert response.status_code == 400

    def test_invalid_body(self):
        response = self.client.generic('GET', path='/geogett/', data=json.dumps(invalid_body),
                                       content_type="application/json")
        assert resp_invalid_body in str(response.content)
        assert response.status_code == 400

    @patch('geogett.views.requests')
    def test_get_happy_path(self, requests_mock):
        requests_get_method_mock = MagicMock()
        requests_get_mock_return_value = MagicMock()
        requests_get_mock_return_value.status_code = 200
        requests_get_mock_return_value.content = ip_stack_mock_response
        requests_get_method_mock.return_value = requests_get_mock_return_value
        requests_mock.get = requests_get_method_mock
        self.client.generic('POST', path='/geogett/', data=json.dumps(valid_body),
                            content_type="application/json")
        response = self.client.generic('GET', path='/geogett/', data=json.dumps(valid_body),
                                       content_type="application/json")
        assert geogett_proper_get_response == response.content
        assert response.status_code == 200

    @patch('geogett.views.requests')
    def test_post_simple_happy_path(self, requests_mock):
        requests_get_method_mock = MagicMock()
        requests_get_mock_return_value = MagicMock()
        requests_get_mock_return_value.status_code = 200
        requests_get_mock_return_value.content = ip_stack_mock_response
        requests_get_method_mock.return_value = requests_get_mock_return_value
        requests_mock.get = requests_get_method_mock
        response = self.client.generic('POST', path='/geogett/', data=json.dumps(valid_body),
                                       content_type="application/json")
        assert "GeoGett added geolocation data for {ip}".format(ip=valid_body['ip']) \
               in str(response.content)
        assert response.status_code == 200

    @patch('geogett.views.requests')
    def test_delete_happy_path(self, requests_mock):
        requests_get_method_mock = MagicMock()
        requests_get_mock_return_value = MagicMock()
        requests_get_mock_return_value.status_code = 200
        requests_get_mock_return_value.content = ip_stack_mock_response
        requests_get_method_mock.return_value = requests_get_mock_return_value
        requests_mock.get = requests_get_method_mock
        self.client.generic('POST', path='/geogett/', data=json.dumps(valid_body),
                            content_type="application/json")
        response = self.client.generic('DELETE', path='/geogett/', data=json.dumps(valid_body),
                                       content_type="application/json")
        assert "GeoGett deleted geolocation data related to {ip}".format(ip=valid_body['ip']) \
               in str(response.content)
        assert response.status_code == 200

    def test_delete_not_found(self):
        response = self.client.generic('DELETE', path='/geogett/', data=json.dumps(valid_body),
                                       content_type="application/json")
        assert 'GeoGett can not delete geolocation data for {ip}. ' \
               'Data for {ip} does not exist'.format(ip=valid_body['ip']) in str(response.content)
        assert response.status_code == 404

    def test_delete_not_url(self):
        response = self.client.generic('DELETE', path='/geogett/', data=json.dumps(valid_body_url),
                                       content_type="application/json")
        assert 'GeoGett can not delete geolocation data for {url}. ' \
               'Data for {url} does not exist'.format(url=valid_body_url['url']) in str(response.content)
        assert response.status_code == 404

    @patch('geogett.views.requests')
    def test_ip_stack_bad_response(self, requests_mock):
        requests_get_method_mock = MagicMock()
        requests_get_mock_return_value = MagicMock()
        requests_get_mock_return_value.status_code = 200
        requests_get_mock_return_value.content = ip_stack_bad_mock_response
        requests_get_method_mock.return_value = requests_get_mock_return_value
        requests_mock.get = requests_get_method_mock
        response = self.client.generic('POST', path='/geogett/', data=json.dumps(valid_body),
                                       content_type="application/json")
        assert resp_ip_stack_bad in str(response.content)
        assert response.status_code == 500
