from django.apps import AppConfig


class GeogettConfig(AppConfig):
    name = 'geogett'
