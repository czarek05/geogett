# GeoGett

Get geolocation data by ip or url.

## Info
It is suggested to use the app with Postman or curl rather than web browser.

## Installing the app with Docker on Linux (tested on Ubuntu 18.04)
Run script `provision_first.sh`. When the script is finished and the app is running, open new terminal and run script `provision_two.sh`. Check if tests passed. The second script creates user - user123 and asks for password. Provide and remember that password. If a problem with the second script occurs, it probably means that containers' names are invalid. Check what names containers have and run commands from the second script manually, with proper names. The app should be available at http://localhost:8000/geogett/

To obtain JWT token perform POST to
http://localhost:8000/api/token/
with JSON body:

{
    "username": "user123",
    "password": "password provided before"
}

If the app was already installed and is turned off, it can be started with:

sudo docker-compose up

## API

It is required that all* requests have JSON body with only one field: "url" or "ip". What is more, requests should have Authorization header with value: Bearer access "token obtained before". Examples of proper bodies:

{
    "ip": "192.152.23.12"
}

{
    "url": "google.com"
}

*there is one exception, if there will be performed GET without body, app will respond with all collected data.
 

GET /geogett

Get chosen or all geolocation data.

POST /geogett

Add geolocation data for provided ip or url.

DELETE /geogett

Delete geolocation data associated with provided ip or url.
